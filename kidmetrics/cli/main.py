import click

from kidmetrics.cli import groups


@click.group()
def kidmetrics():
    """Kidmetrics CLI"""


kidmetrics.add_command(groups.db.db)
kidmetrics.add_command(groups.kids.kids)
kidmetrics.add_command(groups.events.events)
kidmetrics.add_command(groups.api.api)
kidmetrics.add_command(groups.faker.faker)
