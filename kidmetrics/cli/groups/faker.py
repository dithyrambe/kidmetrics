import itertools
import random
import uuid

import click
import pendulum

from kidmetrics.db import models
from kidmetrics.db.models import Kid, Event


def sleep_gen(start, stop, kid):
    current = start
    current = current.add(hours=random.randint(5, 10))
    states = itertools.cycle(("stop", "start"))

    while current < stop:
        state = next(states)
        event = models.Sleep(kid_id=kid.kid_id, state=state)
        event.event_datetime = current.in_tz("UTC")
        event.session.add(event)
        current = current.add(hours=random.randint(5, 10))
    event.session.commit()


def cry_gen(start, stop, kid):
    current = start
    current = current.add(minutes=random.randint(30, 48 * 60))

    while current < stop:
        event = models.Cry(kid_id=kid.kid_id)
        event.event_datetime = current.in_tz("UTC")
        event.session.add(event)
        current = current.add(minutes=random.randint(30, 48 * 60))
    event.session.commit()


def purge_gen(start, stop, kid):
    current = start
    current = current.add(minutes=random.randint(30, 6 * 60))

    while current < stop:
        model = random.choices(
            [models.Pee, models.Poo, models.Reflux],
            weights=[9, 3, 1]
        )[0]
        event = model(kid_id=kid.kid_id)
        event.event_datetime = current.in_tz("UTC")
        event.session.add(event)
        current = current.add(minutes=random.randint(30, 6 * 60))
    event.session.commit()


def weight_gen(start, stop, kid):
    current = start
    current = current.add(days=random.randint(2, 3))
    weight = 3000

    while current < stop:
        weight += random.randint(-20, 50)
        model = models.Weighing
        event = model(kid_id=kid.kid_id, value=str(weight))
        event.event_datetime = current.in_tz("UTC")
        event.session.add(event)
        current = current.add(days=random.randint(2, 3))
    event.session.commit()


def height_gen(start, stop, kid):
    current = start
    current = current.add(days=random.randint(2, 3))
    height = 50

    while current < stop:
        height += random.randint(1, 2)
        model = models.SizeMeasurement
        event = model(kid_id=kid.kid_id, value=str(height))
        event.event_datetime = current.in_tz("UTC")
        event.session.add(event)
        current = current.add(days=random.randint(2, 3))
    event.session.commit()


@click.group()
def faker():
    """Dev related commands to fake data."""


@faker.command()
@click.option(
    "--start",
    type=str,
    required=False,
    default=None,
    help="Start date for faking data."
)
@click.option(
    "--stop",
    type=str,
    required=False,
    default=None,
    help="End date for faking data."
)
@click.option("--nickname", type=str, required=True, help="Fake kid nickname.")
def fake(start, stop, nickname):
    start = pendulum.parse(start) if start else pendulum.now().start_of("month")
    stop = pendulum.parse(stop) if stop else pendulum.now().end_of("month")

    kid_id = f"fake-{uuid.uuid4()}"
    kid = models.Kid(firstname="fakefirstname", lastname="fakelastname", nickname=nickname)
    kid.kid_id = kid_id
    kid.session.add(kid)
    kid.session.commit()

    cry_gen(start, stop, kid)
    sleep_gen(start, stop, kid)
    purge_gen(start, stop, kid)
    weight_gen(start, stop, kid)
    height_gen(start, stop, kid)


@faker.command()
def clean():
    fake_kids = Kid.query.filter(Kid.kid_id.startswith("fake-"))

    Event.query.filter(
        Event.kid_id.in_(
            [_.kid_id for _ in fake_kids.all()]
        )
    ).delete()
    Kid.query.filter(Kid.kid_id.in_([_.kid_id for _ in fake_kids.all()])).delete()
    Event.session.commit()
    Kid.session.commit()
