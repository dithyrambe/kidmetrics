import os

import click
import uvicorn

from kidmetrics.cli.helpers import pass_console


@click.group()
def api():
    """API related commands."""


@api.command()
@click.option("-h", "--host", type=str, required=False, default="127.0.0.1", help="Host to run API")
@click.option("-p", "--port", type=int, required=False, default=8000, help="Port to run API")
@click.option("--reload", type=bool, is_flag=True, default=False, help="Enable auto-reload")
@pass_console
def start(console, host, port, reload):
    host = host or os.getenv("API_HOST", "127.0.0.1")
    port = port or os.getenv("API_PORT", 8000)
    console.print(host, port)

    console.print("[bold yellow]Starting API ...")
    uvicorn.run("kidmetrics.api.app:app", host=host, port=int(port), reload=reload)
