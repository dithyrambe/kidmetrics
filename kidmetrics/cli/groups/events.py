import inspect

import click
import pendulum
from rich.prompt import Prompt

from kidmetrics.cli.helpers import make_table, pass_console
from kidmetrics.db.exceptions import ValidationException
from kidmetrics.db.models import MODELS, Kid


@click.group()
def events():
    """Event related commands."""


@events.command()
@click.option(
    "--since",
    default=None,
    required=False,
    type=str,
    help="Datetime string from which to fetch logs"
)
@click.option(
    "--limit",
    default=None,
    required=False,
    type=int,
    help="Maximum number of metric to fetch"
)
@click.option(
    "--nickname",
    default=None,
    required=False,
    type=str,
    help="Kid nickname"
)
@click.argument("resource")
@pass_console
def log(console: "Console", since: str, limit: int, nickname: str, resource: str):
    if since:
        tz = pendulum.local_timezone().name
        since = pendulum.parse(since, tz=tz).in_tz("UTC").to_iso8601_string()

    event = MODELS.get(resource)
    query = event.query.order_by(event.event_datetime.desc())
    if since:
        query = query.filter(event.event_datetime >= since)
    if nickname:
        kid = Kid.query.filter(Kid.nickname == nickname).first()
        if not kid:
            console.print(
                f"[bold red]:heavy_multiplication_x: Kid '{nickname}' not found."
            )
            exit(1)
        query = query.filter(event.kid_id == kid.kid_id)
    if limit:
        query = query.limit(limit)

    results = query.all()

    table = make_table(results, resource=event)
    console.print(table)


@events.command()
@click.argument("resource")
@pass_console
def put(console: "Console", resource: str):
    model = MODELS.get(resource)
    specs = inspect.signature(model)

    kwargs = {}
    for arg in specs.parameters:
        default = specs.parameters.get(arg).default
        value = Prompt.ask(arg, default=default if default is not inspect._empty else ...)
        kwargs[arg] = value

    with console.status(
            "[yellow] inserting ...",
            spinner="dots"
    ):
        try:
            item = model(**kwargs)
            model.session.add(item)
            model.session.commit()
        except ValidationException as err:
            err_type = err.__class__.__name__
            err_msg, *_ = err.args
            console.print(
                f"[bold red]:heavy_multiplication_x: {err_type}: {err_msg}"
            )
            exit(1)
    console.print("[bold green]:heavy_check_mark: Record inserted successfully.")
