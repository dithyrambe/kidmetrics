import json

import click
from rich.prompt import Confirm, Prompt
from sqlalchemy import or_

from kidmetrics.cli.helpers import make_table, pass_console
from kidmetrics.db.models import Kid
from kidmetrics.db.models.kids import NicknameAlreadyExists


@click.group()
def kids():
    """Kids related commands."""


@kids.command()
@click.option(
    "--firstname",
    default=None,
    required=False,
    type=str,
    help="Firstname filter"
)
@click.option(
    "--lastname",
    default=None,
    required=False,
    type=str,
    help="Lastname filter"
)
@click.option(
    "--nickname",
    default=None,
    required=False,
    type=str,
    help="Nickname filter"
)
@pass_console
def ls(console, firstname, lastname, nickname):
    query = Kid.query
    if firstname:
        query = query.filter(Kid.firstname.ilike(firstname))
    if lastname:
        query = query.filter(Kid.lastname.ilike(lastname))
    if nickname:
        query = query.filter(Kid.nickname.ilike(nickname))

    results = query.all()
    table = make_table(results, Kid)
    console.print(table)


@kids.command()
@click.option(
    "--firstname",
    default=None,
    required=False,
    type=str,
    help="Kid's firstname"
)
@click.option(
    "--lastname",
    default=None,
    required=False,
    type=str,
    help="Kid's lastname"
)
@click.option(
    "--nickname",
    default=None,
    required=False,
    type=str,
    help="Kid's nickname"
)
@pass_console
def put(console, firstname, lastname, nickname):
    kwargs = {
        "firstname": firstname or Prompt.ask("firstname"),
        "lastname": lastname or Prompt.ask("lastname"),
        "nickname": nickname or Prompt.ask("nickname")
    }

    with console.status(
            "[yellow] inserting ...",
            spinner="dots"
    ):
        try:
            kid = Kid(**kwargs)
        except NicknameAlreadyExists as err:
            console.print(f"[bold red] {err}")
            exit(1)

        Kid.session.add(kid)
        Kid.session.commit()
    console.print("[bold green]:heavy_check_mark: Record inserted successfully.")


@kids.command()
@click.option(
    "--id",
    default=None,
    required=False,
    type=str,
    help="Kid's id"
)
@click.option(
    "--nickname",
    default=None,
    required=False,
    type=str,
    help="Kid's nickname"
)
@pass_console
def rm(console, id, nickname):
    if not id and not nickname:
        id = Prompt.ask("Kid id")

    kid = Kid.query.filter(or_(Kid.nickname == nickname, Kid.kid_id == id)).first()
    if kid is None:
        console.print(f"[bold red]Kid not found.")
        exit(0)
    console.print_json(json.dumps(kid.to_dict()))

    if Confirm.ask(
            f"[red bold]Are you sure you want to delete this kid ?",
            default=False
    ):
        Kid.session.delete(kid)
        Kid.session.commit()
        console.print(
            f"[green]:heavy_check_mark: Kid [b]{kid.kid_id}[/b] deleted."
        )
