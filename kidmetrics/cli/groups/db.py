import click
from rich.prompt import Confirm

from kidmetrics.cli.helpers import pass_console


@click.group()
def db():
    """DB related commands."""


@db.command()
@pass_console
def init_db(console: "Console"):
    from kidmetrics.db.connector import engine
    from kidmetrics.db.models.base import Base

    with console.status(
            "[yellow]Initializing DB ...",
            spinner="dots"
    ):
        Base.metadata.create_all(bind=engine)
    console.print("[bold green] DB initialized.")


@db.command()
@click.option("--table", type=str, required=False, help="Table to drop")
@click.option("--all", is_flag=True, help="Whether to drop all tables")
@pass_console
def drop_table(console: "Console", table: str, all: bool):
    from kidmetrics.db.connector import engine
    from kidmetrics.db.models.base import Base

    if all:
        tables = [t for _, t in Base.metadata.tables.items()]
    else:
        table = Base.metadata.tables.get(table)
        if table is None:
            console.print(f"[yellow]There is no '{table}' table.")
            exit(1)
        tables = [table]

    console.print("[yellow bold]:warning: You're about to perform a destructive task")
    if Confirm.ask(
            f"[red bold]Delete tables [i]{'/'.join([_.name for _ in tables])}[/i] ?",
            default=False
    ):
        if Confirm.ask("[red bold]Are you sure ?", default=False):
            for table in tables:
                table.drop(bind=engine, checkfirst=engine)
            console.print(
                f"[green bold]Tables [i]{'/'.join([_.name for _ in tables])}[/i] deleted."
            )
