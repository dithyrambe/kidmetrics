import datetime
from functools import wraps
from typing import Callable

import pendulum
import sqlalchemy
from rich.console import Console
from rich.table import Table


def pass_console(f: Callable):
    """
    Helper decorator to pass a rich console as extra argument.

    Args:
        f (Callable): Function to be decorated.

    Returns:
        Callable: Decorated func with a console as first arg.

    """
    console = Console()

    @wraps(f)
    def wrapper(*args, **kwargs):
        return f(console, *args, **kwargs)

    return wrapper


def make_table(results: list, resource: "Base"):
    """
    Build a table from results of query.

    Args:
        results (list): List of records to display.
        resource (Base): Base resource from sqlalchemy.

    Returns:
        Table: Rich table to be displayed in console.

    """

    schema = sqlalchemy.inspect(resource)

    table = Table(title=f"metric: {resource.__name__}")

    for column in schema.columns.keys():
        table.add_column(column, style="green")

    for row in results[::-1]:
        record = row.to_dict()
        record = {
            k: pendulum.instance(v, tz="UTC").to_iso8601_string()
            if isinstance(v, datetime.datetime) else v
            for k, v in record.items()
        }
        table.add_row(*[record[col] for col in schema.columns.keys()])

    return table
