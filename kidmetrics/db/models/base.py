import uuid
from typing import Optional

import pendulum
from pendulum.parsing.exceptions import ParserError
from sqlalchemy import Column, String, func, DateTime
from sqlalchemy.ext.declarative import declarative_base

from kidmetrics.db.connector import Session
from kidmetrics.db.exceptions import MalformedDatetime

LOCALE_TZ = "Europe/Paris"


class _Base:
    session = Session()

    @classmethod
    @property
    def query(cls):
        query = cls.session.query(cls)
        return query


Base = declarative_base(cls=_Base)


class BaseEvent(Base):
    __tablename__ = "events"

    event_id = Column(String, primary_key=True, default=lambda: str(uuid.uuid4()))
    event_datetime = Column(DateTime, default=lambda: pendulum.now("UTC"))
    kid_id = Column(String)
    event_type = Column(String)
    state = Column(String)
    value = Column(String)
    value_type = Column(String)

    def __init__(
            self,
            kid_id: str,
            event_type: str,
            state: str,
            value: str,
            value_type: str,
    ):
        self.kid_id = kid_id.strip() if kid_id else None
        self.event_type = event_type.strip() if event_type else None
        self.state = state.strip() if state else None
        self.value = value.strip() if value else None
        self.value_type = value_type.strip() if value_type else None

    @classmethod
    def query_between_dt(cls, start: Optional[str] = None, stop: Optional[str] = None):
        try:
            start = pendulum.parse(start, tz=LOCALE_TZ) if start else None
            stop = pendulum.parse(stop, tz=LOCALE_TZ) if stop else None
        except ParserError:
            raise MalformedDatetime("Either start or stop datetime string is malformed.")

        query = cls.query
        if start:
            query = query.filter(cls.event_datetime >= start)
        if stop:
            query = query.filter(cls.event_datetime < stop)
        return query
