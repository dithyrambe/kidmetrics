from .base import Base
from .kids import Kid
from .events import (
    Event,
    Weighing,
    SizeMeasurement,
    Fever,
    Purge,
    Pee,
    Poo,
    Reflux,
    Sleep,
    SleepStart,
    SleepStop,
    Feeding,
    FeedingStart,
    FeedingStop,
    Cry,
)

MODELS = {
    model.__name__: model
    for model in {
        Event,
        Weighing,
        SizeMeasurement,
        Fever,
        Purge,
        Pee,
        Poo,
        Reflux,
        Sleep,
        SleepStart,
        SleepStop,
        Feeding,
        FeedingStart,
        FeedingStop,
        Cry,
    }
}
