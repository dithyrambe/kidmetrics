from typing import Optional

from kidmetrics.db.models.base import BaseEvent
from kidmetrics.db.models.kids import Kid
from kidmetrics.db.exceptions import KidNotFound, SuccessiveEventsShareSameState


class Event(BaseEvent):
    EVENT_TYPE = None
    VALUE_TYPE = None
    STATE = None

    def __init__(self, kid_id: str, state: Optional[str], value: Optional[str]):
        BaseEvent.__init__(
            self,
            kid_id=kid_id,
            event_type=self.EVENT_TYPE,
            state=state,
            value=value,
            value_type=self.VALUE_TYPE,
        )

        self._validate()

    def _validate(self):
        kid = Kid.query.filter(Kid.kid_id == self.kid_id).first()
        if kid is None:
            raise KidNotFound(f"Kid '{self.kid_id}' not found.")

    @classmethod
    @property
    def query(cls):
        query = cls.session.query(cls)
        if cls.EVENT_TYPE:
            query = query.filter(cls.event_type == cls.EVENT_TYPE)
        if cls.STATE:
            query = query.filter(cls.state == cls.STATE)
        return query

    def to_dict(self, sparse: bool = False):
        if sparse:
            return {k: v for k, v in self.to_dict().items() if v is not None}
        return {
            "event_id": self.event_id,
            "event_datetime": self.event_datetime,
            "kid_id": self.kid_id,
            "event_type": self.event_type,
            "state": self.state,
            "value": self.value,
            "value_type": self.value_type,
        }


class TimeBoxedEvent(Event):
    def _validate(self, klass=None):
        Event._validate(self)

        klass = klass or self.__class__
        query = klass.query.filter(
            klass.kid_id == self.kid_id
        ).order_by(
            klass.event_datetime.desc()
        )

        last = query.first()
        if last and last.state == self.state:
            raise SuccessiveEventsShareSameState(
                f"Last {klass.__name__} of {self.kid_id} is already in state '{self.state}'."
            )


class Weighing(Event):
    """
    Weighing event. Value corresponds to weights in grams.
    """
    EVENT_TYPE = "weighing"
    VALUE_TYPE = "float"
    STATE = None

    def __init__(self, kid_id, value):
        Event.__init__(self, kid_id=kid_id, state=Weighing.STATE, value=value)


class SizeMeasurement(Event):
    """
    Size measurement event. Value corresponds to size in cm.
    """
    EVENT_TYPE = "sizeMeasurement"
    VALUE_TYPE = "float"
    STATE = None

    def __init__(self, kid_id, value):
        Event.__init__(self, kid_id=kid_id, state=SizeMeasurement.STATE, value=value)


class Fever(Event):
    """
    Fever event. Value corresponds to temperature in °C.
    """
    EVENT_TYPE = "fever"
    VALUE_TYPE = "float"

    def __init__(self, kid_id, value):
        Event.__init__(self, kid_id=kid_id, state=None, value=value)


class Purge(Event):
    """
    Purge event. State is the type of purge.
    """
    EVENT_TYPE = "purge"
    VALUE_TYPE = "float"


class Poo(Purge):
    """
    Poo event.
    """
    STATE = "poo"

    def __init__(self, kid_id):
        Purge.__init__(self, kid_id=kid_id, state=Poo.STATE, value=None)


class Pee(Purge):
    """
    Pee event.
    """
    STATE = "pee"

    def __init__(self, kid_id):
        Purge.__init__(self, kid_id=kid_id, state=Pee.STATE, value=None)


class Reflux(Purge):
    """
    Reflux event.
    """
    STATE = "reflux"

    def __init__(self, kid_id):
        Purge.__init__(self, kid_id=kid_id, state=Reflux.STATE, value=None)


class Feeding(TimeBoxedEvent):
    """Feeding event. State is start/end of feeding. Value is volume remaining in ml."""
    EVENT_TYPE = "feeding"
    VALUE_TYPE = "float"


class FeedingStart(Feeding):
    """
    Start feeding event. Value is volume in ml.
    """
    STATE = "start"

    def __init__(self, kid_id, value):
        Feeding.__init__(self, kid_id=kid_id, state=FeedingStart.STATE, value=value)

    def _validate(self, klass=None):
        Feeding._validate(self, klass=Feeding)


class FeedingStop(Feeding):
    """
    Stop feeding event. Value is remaining volume in ml.
    """
    STATE = "stop"

    def __init__(self, kid_id, value):
        Feeding.__init__(self, kid_id=kid_id, state=FeedingStart.STATE, value=value)

    def _validate(self, klass=None):
        Feeding._validate(self, klass=Feeding)


class Sleep(TimeBoxedEvent):
    """
    Sleeping event.
    """
    EVENT_TYPE = "sleep"

    def __init__(self, kid_id, state):
        Event.__init__(self, kid_id=kid_id, state=state, value=None)


class SleepStart(Sleep):
    """
    Bed time event.
    """
    STATE = "start"

    def __init__(self, kid_id):
        Sleep.__init__(self, kid_id=kid_id, state=SleepStart.STATE)

    def _validate(self, klass=None):
        Sleep._validate(self, klass=Sleep)


class SleepStop(Sleep):
    """
    Bed time event.
    """
    STATE = "stop"

    def __init__(self, kid_id):
        Sleep.__init__(self, kid_id=kid_id, state=SleepStop.STATE)

    def _validate(self, klass=None):
        Sleep._validate(self, klass=Sleep)


class Cry(Event):
    EVENT_TYPE = "cry"

    def __init__(self, kid_id):
        Event.__init__(self, kid_id=kid_id, state=None, value=None)
