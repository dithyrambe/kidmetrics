import uuid

from sqlalchemy import Column, String

from .base import Base
from ..exceptions import ValidationException


class NicknameAlreadyExists(ValidationException):
    """Raises when Kid's nickname already exists in DB"""


class Kid(Base):
    __tablename__ = "kids"

    kid_id = Column(String, primary_key=True, default=uuid.uuid4)
    firstname = Column(String)
    lastname = Column(String)
    nickname = Column(String)

    def __init__(self, firstname: str, lastname: str, nickname: str):
        self.firstname = firstname.capitalize().strip()
        self.lastname = lastname.capitalize().strip()
        self.nickname = nickname.strip()

        self._validate()

    def _validate(self):
        kids_with_same_nickname = Kid.query.filter(Kid.nickname == self.nickname).all()
        if kids_with_same_nickname:
            raise NicknameAlreadyExists(f"Nickname '{self.nickname}' already exists in DB.")

    def to_dict(self):
        return {
            "kid_id": self.kid_id,
            "firstname": self.firstname,
            "lastname": self.lastname,
            "nickname": self.nickname
        }
