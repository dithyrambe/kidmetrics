import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

DB_NAME = "kidmetrics"
engine = create_engine(
    f"postgresql://"
    f"{os.getenv('POSTGRE_USER')}:{os.getenv('POSTGRE_PASSWORD')}"
    f"@{os.getenv('POSTGRE_HOST')}:{os.getenv('POSTGRE_PORT')}"
    f"/{DB_NAME}"
)

Session = sessionmaker(bind=engine)
