class KidMetricException(Exception):
    """Base exception."""


class ValidationException(KidMetricException):
    """Raises when a model can't be validate after instantiation"""


class SuccessiveEventsShareSameState(ValidationException):
    """Raises when two time-boxed events share the same state."""


class KidNotFound(KidMetricException):
    """Raises when kid is not found in DB."""


class MalformedDatetime(KidMetricException):
    """Raises when datetime string is not properly formed"""
