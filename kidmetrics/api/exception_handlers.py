from fastapi.responses import JSONResponse


async def default_handler(request, exc):
    err_type = exc.__class__.__name__
    msg, *_ = exc.args
    return JSONResponse(
        status_code=400,
        content={
            "err": err_type,
            "detail": f"{msg}"
        }
    )
