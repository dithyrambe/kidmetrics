from fastapi import FastAPI

from kidmetrics.api.routers import kids, events
from kidmetrics.db.exceptions import KidMetricException

from kidmetrics.api.exception_handlers import default_handler


app = FastAPI()
app.router.include_router(kids.router)
app.router.include_router(events.router)

app.add_exception_handler(KidMetricException, default_handler)
