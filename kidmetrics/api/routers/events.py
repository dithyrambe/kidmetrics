import inspect

from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse

from kidmetrics.db import models
from kidmetrics.db.models import Kid

router = APIRouter(prefix="/events", default_response_class=JSONResponse)


@router.get("")
def event_types():
    """Lists events names"""
    return {
        "event_names": sorted([*models.MODELS])
    }


@router.get("/{event_name}")
def list_metrics(event_name: str, start: str = None, stop: str = None):
    """List all events of a specific type."""
    klass = models.MODELS.get(event_name)
    if klass is None:
        raise HTTPException(
            status_code=404,
            detail=f"Resource {event_name} not found."
        )
    query = klass.query_between_dt(start, stop)
    results = query.order_by(klass.event_datetime).all()
    events = [elt.to_dict(sparse=True) for elt in results]
    return {
        "events": events
    }


@router.get("/{event_name}/{kid_nickname}")
def list_metrics(event_name: str, kid_nickname: str, start: str = None, stop: str = None):
    """List all events of a specific type for a specific kid."""

    kid = Kid.query.filter(Kid.nickname == kid_nickname).first()
    if kid is None:
        raise HTTPException(
            status_code=404,
            detail=f"Kid '{kid_nickname}' not found."
        )

    klass = models.MODELS.get(event_name)
    if klass is None:
        raise HTTPException(
            status_code=404,
            detail=f"Resource {event_name} not found."
        )
    query = klass.query_between_dt(start, stop)
    query = query.filter(klass.kid_id == kid.kid_id)
    results = query.order_by(klass.event_datetime).all()
    events = [elt.to_dict(sparse=True) for elt in results]
    return {
        "events": events
    }


@router.post("/{event_name}/{kid_nickname}")
def register_event(event_name: str, kid_nickname: str, payload: dict):
    """Register new event."""

    kid = Kid.query.filter(Kid.nickname == kid_nickname).first()
    if kid is None:
        raise HTTPException(
            status_code=404,
            detail=f"Kid '{kid_nickname}' not found."
        )

    klass = models.MODELS.get(event_name)
    if klass is None:
        raise HTTPException(
            status_code=404,
            detail=f"Resource {event_name} not found."
        )

    signature = inspect.signature(klass)
    expected_keys = set(signature.parameters) - {"kid_id"}
    actual_keys = set(payload)
    if expected_keys != actual_keys:
        raise HTTPException(
            status_code=400,
            detail=f"Enable to create event. Payload should present {expected_keys}. "
                   f"Got {actual_keys}"
        )

    event = klass(kid_id=kid.kid_id, **payload)
    klass.session.add(event)
    klass.session.commit()
    return event.to_dict(sparse=True)
