from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse

from kidmetrics.db.models import Kid

router = APIRouter(prefix="/kids", default_response_class=JSONResponse)


@router.get("")
def list_kids(firstname: str = None, lastname: str = None, nickname: str = None):
    """Lists kids."""
    query = Kid.query
    if firstname:
        query = query.filter(Kid.firstname.ilike(firstname))
    if lastname:
        query = query.filter(Kid.lastname.ilike(lastname))
    if nickname:
        query = query.filter(Kid.nickname.ilike(nickname))
    results = query.all()
    return {
        "results": [kid.to_dict() for kid in results]
    }


@router.get("/{nickname}")
def get_kid(nickname):
    kid = Kid.query.filter(Kid.nickname == nickname).first()
    if not kid:
        raise HTTPException(
            status_code=404,
            detail="Kid not found."
        )
    return kid.to_dict()
